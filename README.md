# C to Python wrapper

To build this example, I've followed tutorials and codes from:

- [Cython wiki](https://github.com/cython/cython/wiki/tutorials-NumpyPointerToC)
- [Geeks for geeks cython tutorial](https://www.geeksforgeeks.org/cython-to-wrap-existing-c-code/)
- [GaelVaroquaux Cython example](https://gist.github.com/GaelVaroquaux/1249305/3b2990b3f320b95bff91a471ac37c0112951ff10)
- [Stavshamir wrapper](https://stavshamir.github.io/python/making-your-c-library-callable-from-python-by-wrapping-it-with-cython/)

## How are the files organized?

**Folder: CodeC**

This folder contains the C code for the library: `libexample.c` and `libexample.h`. The input/output of this library is:

- INPUT
  - `const char *name`: a string to be printed on screen
  - `int n`: the size of the array `vec`
  - `double d`: a real number
  - `double *vec`: a array
- OUTPUT
  - allocates and returns an array `a = d*vec`
  - modifies `vec = -vec` in place

The `makefile` compiles the library into `libexample.a`. We compile as static so it can be later incorporated into the full cython wrapper.

**File: setup.py**

Defines the wrapper parameters. Note that the `libraries=["example"]` entry tells cython to look for a file `libexample.a` first in `library_dirs` and latter on the system path if need. If your C does uses other libraries, you may need to extend this list, for instance: `libraries=["example", "fftw3", "lapacke", "blas", "lapack"]`.

**File: wrapper.pyx**

This is the actual wrapper. It defines the functions that will actually be accessible from python's end. In this case, only the `py_hello` function. This function calls the C function `hello` and translate the input/output formats between python and C. Notice that the C code allocates an array and returns to python. You cannot *free* this array within C or the information will be lost, so we must call *free* within the wrapper, but before that, we copy the C array into a numpy array.

**File: makefile**

The `makefile` calls the setup.py to build the library and generate the file `mylib.cpython-38-x86_64-linux-gnu.so`. The name `mylib` is defined in setup.py. This is the final binary (ELF) of the library. To include it on your python code, call `import mylib` as in the example testing.py. 

To be imported, the file `mylib.cpython-38-x86_64-linux-gnu.so` must be either in the same folder of your python script, or you can append the path to find it into python's search list with

```python
import sys
sys.path.append("/path/to/mylib/")
```

