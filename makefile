LIB_DIR = CodeC

default: clib wrapper

wrapper: setup.py wrapper.pyx 
	@echo "==== RUN python setup"
	@python3 setup.py build_ext --inplace 
	@echo "==== REMOVE TEMPORARY FILES"
	@rm -Rf wrapper.c build

clib:
	@make -C $(LIB_DIR)

clean:
	@echo "==== CLEAN ROOT"
	@make -C $(LIB_DIR) clean
	@rm -Rf *.so
	@rm -Rf wrapper.c build
