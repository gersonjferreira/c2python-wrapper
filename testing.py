import numpy as np
import mylib as ml
print("------------------")

inp = np.array([1.0,2.0,3.0,4.0])
out = ml.py_hello(b"world", 4, 1/4, inp)

print("------------------")
print("input :", inp)
print("output:", out)
print("------------------")
