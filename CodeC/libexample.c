#include <stdio.h>
#include <stdlib.h>
#include "libexample.h"

double *hello(const char *name, int n, double d, double *vec) {
    double *a = (double *) calloc(n, sizeof(double));
    printf("hello %s\n", name);
    for (int i=0; i<n; i++) {
        a[i] = d*vec[i];
        vec[i] = -vec[i];
    }

    return(a);
}
